FROM nginx
RUN apt-get update && apt-get install git -y

# Git repo for build
ARG git_branch="master"
ARG gitlab_token_name="gitlab+deploy-token-132823"
ARG gitlab_token_password="fzmi_zyyYChRwkaqoix3"
ARG git_repo_path="agepoly/it/dev/4ltrophy"
ARG git_temp_path="/tmp/4ltrophy"
ARG pip_reqs=""

RUN git clone  --progress --verbose --single-branch  --branch $git_branch https://$gitlab_token_name:$gitlab_token_password@gitlab.com/$git_repo_path $git_temp_path

RUN find  "$git_temp_path"
RUN cp -r $git_temp_path/4ltrophy /usr/share/nginx/html
COPY ./4ltrophy /usr/share/nginx/html
